'use strict';

const chai = require('chai');
const sinon = require('sinon');
const expect = require('chai').expect;
const path = require('path');
const PathResolver = require('../src/resolver.js');
const pathResolver = new PathResolver();

describe('PathResolver', () => {
	it('should be a function', () => {
		expect(PathResolver).to.be.a('function');
	});

	describe('Instance', () => {
		it('should be an object', () => {
			expect(pathResolver).to.be.an('object');
		});

		describe('resolve Fn', () => {
			it('should return a fully resolved path, when given a module name', () => {
				let value = 'mocha', resolved = pathResolver.resolve(value);
				expect(resolved).to.be.a('string');
				expect(resolved).to.equal(path.resolve(__dirname, '..', 'node_modules', 'mocha', 'index.js'));
			});

			it('should return a fully resolved path, when given a file name', () => {
				let value = '../src/file.js', resolved = pathResolver.resolve(value);
				expect(resolved).to.be.a('string');
				expect(resolved).to.equal(path.resolve(__dirname, '..', 'src', 'file.js'));
			});

			it('should return the string passed into \'value\' as a non-fully resolved path, when given an incorrect input', () => {
				let value = 'this isn\'t a fully qualified path', resolved = pathResolver.resolve(value);
				expect(resolved).to.equal(value);
			});
		});
	});
})

