'use strict';

const sinon = require('sinon');
const expect = require('chai').expect;

const fs = require('fs');
const path = require('path');
const Config = require('../src/config.js');
const File = require('../src/file.js');
const PathResolver = require('../src/resolver.js');

const config = new Config();

const testObject = {
	property1: 1,
	property2: 2
};
const testObject2 = {
	property3: 3,
	property4: 4
};
const testObject3 = {
	property5: 5,
	property6: 6
};
const mergedObject = {
	property3: 3,
	property4: 4,
	property5: 5,
	property6: 6
};
const testString = 'test string';
const testArray = [1, 2, 3];

const fileConfig = {
	resolve: {
		root: path.join(__dirname, 'build'),
		prefix: '',
		filename: '[theme].[hash]',
		ext: '.css'
	},
	output: {
		path: './'
	},
	manifest: {
		path: path.join(__dirname, './build/manifests'),
		filename: 'css_manifest.json'
	},
	renderOptions: {
		paths: [
			path.join(__dirname, './build'),
			path.join(__dirname, './build/public')
		],
		compress: true
	},
	theme: {
		path: __dirname,
		filename: 'dt-test-theme.less'
	},
	variables: {
		imgPath: '/images'
	},
	manifestOutput: { 'test': 'manifest' },
	files: [
		'testStyles.less'
	]
};

describe('Config', () => {
	it('should be a function', () => {
		expect(Config).to.be.a('function');
	});

	describe('instance', () => {
		config.configName = 'file.js';
		it('should be an object', () => {
			expect(config).to.be.an('object');
		});

		describe('get pathResolver fn', () => {
			it('should return an object', () => {
				expect(config).to.be.an('object');
			});
		});

		describe('find fn', () => {
			let pattern = '../src/file.js',
				found = config.find(pattern);
			it('should return a string', () => {
				expect(found).to.be.a('string');
			});

			it('should return a full file path', () => {
				expect(found).to.equal(path.resolve(__dirname, '..', 'src', 'file.js'));
			});

			it('should return null, if filename does not equal the configName property\'s value', () => {
				let pattern = './build/testLessFile.css',
					found = config.find(pattern);
				expect(found).to.be.null;
			});
		});

		describe('findClosest fn', () => {
			it('should return a fully resolved path', () => {
				let pattern = '../src/file.js';
				expect(config.findClosest(pattern)).to.be.a('string')
					.and.to.equal(path.resolve(__dirname, '..', 'src', 'file.js'));
			});

			it('should return a fully resolved path', () => {
				let pattern = '../src/file';
				expect(config.findClosest(pattern)).to.be.a('string')
					.and.to.equal(path.resolve(__dirname, '..', 'src', 'file.js'));
			});

			it('Throw Error if Config is not found', () => {
				let pattern = '../build/file.txt';
				expect(() => config.findClosest(pattern)).to.throw('Config Not Found: Expecting file.js');
			});

			it('Throw Error if Config is not resolveable', () => {
				let pattern = 'file';
				expect(() => config.findClosest(pattern)).to.throw('Config Not Found: Expecting file.js');
			});
		});

		describe('prep fn', () => {
			var prep_config = new Config();
			sinon.stub(prep_config, 'findClosest', function(x) {
				return path.join(__dirname, 'build/testManifest.json');
			});
			it('if passed a pathway to an object it should return expected object', () => {

				expect(prep_config.prep('./build/testFile.js')).to.be.an('object');
				prep_config.findClosest.restore();
			});
		});

		describe('getThemes fn', () => {
			let retrievedThemes = config.getThemes();
			it('should return an array', () => {
				expect(retrievedThemes).to.be.an('array');
			});

			config.theme = {};
			it('should return the number of filenames in the requested array', () => {
				expect(config.getThemes(path.resolve(__dirname, '..', 'src')).length).to.equal(3);
			});

			it('should return an empty array, if no theme or theme path are given', () => {
				expect(config.getThemes('').length).to.equal(0);
			});
		});

		describe('clone fn', () => {
			let obj = testObject,
				cloned = config.clone(obj);
			it('should return an object, if \'obj\' is an object', () => {
				expect(cloned).to.be.an('object');
			});

			it('should return null if \'obj\' is null', () => {
				let obj = null,
					cloned = config.clone(obj);
				expect(cloned).to.be.null;
			});

			it('should return an array, if \'obj\' is an array', () => {
				let arr = [],
					cloned = config.clone(arr);
				expect(cloned).to.be.an('array');
			});

			context('\'obj\' is an array', () => {
				it('should equal an empty array', () => {
					let arr = testArray,
						cloned = config.clone(arr);
					expect(cloned).to.deep.equal([1, 2, 3]);
				});
			});

			context('\'obj\' is a string', () => {
				it('should return what was passed in', () => {
					let obj = 'not an object',
						cloned = config.clone(obj);
					expect(cloned).to.equal('not an object');
				});
			});
		});

		describe('set fn', () => {
			let obj = testObject,
				key = 'property1',
				hasBeenSet = config.set(obj, key);

			context('\'key\' exists as a property of \'obj\'', () => {
				it('should return 1', () => {
					expect(hasBeenSet).to.deep.equal(testObject);
				});
			});

			it('should return an object', () => {
				expect(hasBeenSet).to.be.an('object');
			});

			context('\'key\' exists in obj', () => {
				it('should return obj', () => {
					expect(hasBeenSet).to.deep.equal(obj);
				});
			});

			context('\'key\' does not exist in obj, but \'key\' is not null', () => {
				it('should return obj', () => {
					key = 'property203';
					expect(hasBeenSet).to.deep.equal(obj);
				});
			});

			context('\'key\' is null', () => {
				it('should return obj', () => {
					key = null;
					expect(hasBeenSet).to.deep.equal(obj);
				});
			});
			describe('obj param', () => {
				it('should be an object', () => {
					expect(obj).to.be.an('object');
				});
			});

			describe('key param', () => {
				it('should be a string', () => {
					key = 'property1';
					expect(key).to.be.a('string');
				});
			});
		});

		describe('merge fn', () => {
			let src = testObject2,
				obj = testObject3,
				merged = config.merge(src, obj);

			it('should return an object', () => {
				expect(merged).to.be.an('object');
			});

			it('should merge the 2 objects', () => {
				expect(merged).to.deep.equal(mergedObject);
			});

			describe('src param', () => {
				it('should be an object', () => {
					expect(src).to.be.an('object');
				});
			});

			describe('obj param', () => {
				it('should be an object', () => {
					expect(obj).to.be.an('object');
				});
			});
		});

		describe('process fn', () => {

			let config;
			let setter;
			let getThemes;

			beforeEach(function(done) {
				config = new Config();

				getThemes = sinon.stub(config, 'getThemes', function() {
					return ['build'];
				});

				sinon.stub(fs, 'readdirSync', function(path) {
					return ['dt-test-theme.less'];
				});
				done();
			});

			afterEach(function(done) {
				config.getThemes.restore();
				fs.readdirSync.restore();
				done();
			});


			it('should return a promise', () => {
				expect(config.process(fileConfig)).to.be.a('promise');
			});

			it('should reject the promise if there is an error', () => {
				return config.process(fileConfig).catch(e => {
					expect(e).to.be.instanceOf(Error);
				});
			});

			it('should call needed methods based on payload', () => {
				return config.process(fileConfig).then(result => {
					expect(result[0][0]).to.be.an('object')
						.and.itself.to.have.property('test')
						.and.to.equal('manifest');
					expect(getThemes.calledOnce).to.equal(true);
				});
			});

			it('should call needed methods based on payload - no getThemes', () => {
				let noTheme = Object.assign({}, fileConfig);
				delete noTheme.theme;

				return config.process(noTheme).then(result => {
					expect(getThemes.calledOnce).to.equal(false);
				});
			});
		});

		describe('saveManifest fn', () => {
			let filename = 'testStyles.less';

			context('all three params are valid', () => {
				it('should return a string', () => {
					config.defaults.manifestOutput = { 'test': 'manifest' }
					config.defaults.manifest.path = path.join(__dirname, './build'); 
					config.defaults.manifest.filename = 'testManifest.json';
					return config.saveManifest().then(data => {
						expect(data).to.be.a('object')
							.and.itself.to.have.property('test')
							.and.to.equal('manifest');
					});
				});
			});
			
			it('should reject the promise if there is an error', () => {
				sinon.stub(fs, 'writeFile', function(x,y,cb){
					return cb(new Error('write error'));
				}); 

				return config.saveManifest('test.file').catch((err) => {
					expect(err).to.be.an('error');
					fs.writeFile.restore();
				});
			});
		});
	});
});