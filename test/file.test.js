'use strict'

const path = require('path');
const fs = require('fs');
const expect = require('chai').expect;
const sinon = require('sinon');
const less = require('less');
const File = require('../src/file.js');
const Config = require('../src/config.js');

const THEME_ROOT = path.join(__dirname, 'build');

const fileConfig = {
	resolve: {
		root: THEME_ROOT,
		prefix: '',
		filename: '[theme].[hash]',
		ext: '.css'
	},
	output: {
		path: './'
	},
	manifest: {
		path: path.join(__dirname, './build/manifests'),
		filename: 'css_manifest.json'
	},
	renderOptions: {
		paths: [
			path.join(__dirname, './build'),
			path.join(__dirname, './build/public')
		],
		compress: true
	},
	theme: {
		path: __dirname,
		filename: 'dt-test-theme.less'
	},
	variables: {
		imgPath: '/images'
	},
	manifestOutput: {'test':'manifest'},
	files: [
		'testStyles.less'
	]
}

describe('File', () => {
	it('should be a function', () => {
		expect(File).to.be.a('function');
	});

	describe('Instance', () => {
		it('should be an object', () => {
			const instance = new File(fileConfig);
			expect(instance).to.be.an('object');
		});

		describe('get config fn', () => {
			const instance = new File(fileConfig);

			it('should equal param', () => {
				const result = instance.config;
				expect(result).to.deep.equal(fileConfig);
			});
		});

		describe('read fn', () => {
			const instance = new File(fileConfig);
			
			it('should return a string', () => {
				const result = instance.read(__filename);
				expect(result).to.be.a('string');
			});

		});
		
		describe('write fn', function() {
			const instance = new File(fileConfig);
			let filename = 'testLessFile.css',
				content = 'test1',
				theme = 'test2',
				filePath = path.resolve(__dirname, 'build/', filename);

			before(function (done) {
				sinon.stub(instance, 'addToManifest', function() {
					return true;
				});
				done();
			});

			after(function (done) {
				instance.addToManifest.restore();
				done();
			});

			it('should return a promise', () => {
				expect(instance.write()).to.be.a('promise');
			});

			it('should write to a new file at the given path', function() {
				return instance.write(filePath, filename, content, theme).then((result) => {
					expect(result).to.be.a('string');
					expect(result).to.be.equal(filePath);
				});
			});

			it('should trigger reject on error', function() {
				sinon.stub(fs, 'writeFile', function(x,y,cb){
					return cb(new Error('write error'));
				}); 
				return instance.write(filePath, filename, content, theme).catch((e) => {
					expect(e).to.be.instanceOf(Error);
					fs.writeFile.restore();
				});
			});

		});
		
		describe('addToManifest fn', () => {

			let newConfig2 = Object.assign({}, fileConfig, {
				'testStyles.less': fileConfig
			});
			let instance = new File(newConfig2);
			
			before(function (done) {
				sinon.stub(instance, 'formatFilename', function(filename) {
					return filename;
				});
				done();
			});

			after(function (done) {
				instance.formatFilename.restore();
				done();
			});

			it('should return an object', () => {
				let result = instance.addToManifest('testStyles.less', path.resolve(__dirname, 'default.testStyles.less'),'default');
				expect(result).to.be.an('object')
					.and.itself.to.have.property('testStyles.less')
					.and.to.equal('default.testStyles.less');
				});
		});

		describe('createHash fn', () => {
			const instance = new File(fileConfig);
			let test = instance.createHash('test');
			it('should return a string', () => {
				expect(test).to.be.a('string');
			});
			
			it('should create a hash for the given content', () => {
				expect(test).to.equal('098f6bcd4621d373cade4e832627b4f6');
			});
		});

		describe('defineFilename fn', () => {

			before(function (done) {
				sinon.stub(instance, 'createHash', function() {
					return 12345;
				});
				done();
			});

			after(function (done) {
				instance.createHash.restore();
				done();
			});

			let newConfig2 = Object.assign({}, fileConfig, {
				resolve: {
					prefix: 'doh',
					ext: '.css',
					filename: '[theme].[name]'
				}
			});
			let instance = new File(newConfig2);

			it('should return a string as theme.name.css', () => {
				let result = instance.defineFilename('testStyles.less', 'Green', 'teststuff');

				expect(result).to.be.a('string')
					.and.to.equal('Green.testStyles.css');
			});

			it('should return a string as theme.name.hash.css', () => {
				instance.config.resolve.filename = '[theme].[name].[hash]';

				let result = instance.defineFilename('testStyles.less', 'Green', 'teststuff');

				expect(result).to.be.a('string')
					.and.to.equal('Green.testStyles.12345.css');
			});

			it('should return a string as theme.prefix.name.hash.css', () => {
				instance.config.resolve.filename = '[theme].[prefix].[name].[hash]';
				
				let result = instance.defineFilename('testStyles.less', 'Green', 'teststuff');

				expect(result).to.be.a('string')
					.and.to.equal('Green.doh.testStyles.12345.css');
			});

			it('should return a string as hash.prefix.name.hash.css', () => {
				instance.config.resolve.filename = '[hash].[prefix].[name].[hash]';
				
				let result = instance.defineFilename('testStyles.less', 'Green', 'teststuff');

				expect(result).to.be.a('string')
					.and.to.equal('12345.doh.testStyles.12345.css');
			});
		
		});

		describe('render fn', () => {
			let newConfig2 = Object.assign({}, fileConfig, {
				resolve: {
					root: THEME_ROOT,
					prefix: '',
					filename: '[theme].[hash]',
					ext: '.css',
					content : '.alert-info {color : \'blue\';}', 
					theme : 'toyota'
				}
			});
			var instance = new File(newConfig2);
			
			it('should return a string', () => {
				return instance.render('testStyles.less', 
									   newConfig2.resolve.content, 
									   newConfig2.resolve.theme)
					.then((css) => {
						expect(css).to.be.a('string');
				})
			});

			it('should equal the content input', () => {
				return instance.render('testStyles.less', 
									   newConfig2.resolve.content, 
									   newConfig2.resolve.theme)
					.then((css) => {
						expect(css).to.equal('.alert-info{color:\'blue\'}');
				})
			});

			it('should reject the promise if there is a Less render error', () => {
				sinon.stub(less, 'render', function(x,y,cb){
					return cb(new Error('less render error'));
				}); 

				return instance.render('testStyles.less', newConfig2.resolve.content, newConfig2.resolve.theme).catch((err) => {
						expect(err).to.be.instanceOf(Error);
						less.render.restore();
				});
			});

			context('\'theme\' is null', () => {
				it('should return the given css', () => {
					return instance.render('testStyles.less', 
										   newConfig2.resolve.content, 
										   null)
						.catch((e) => {
							expect(e).to.be.instanceOf(Error);
					});
				});
			});
		});

		describe('build fn', () => {

			it('should return a string without Theme - exist in content', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles2.less': fileConfig
				});

				let instance = new File(newConfig2);
				let filename = 'testStyles.less',
					theme = 'build';

				let result = instance.build('testStyles2.less', theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'testStyles2.less')}";`);
			});	

			it('should return a string without imports - exist in content', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles2.less': fileConfig
				});
				newConfig2.imports = ['testImport'];

				let instance = new File(newConfig2);
				let filename = 'testStyles.less',
					theme = 'build';

				let result = instance.build('testStyles2.less', theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'testStyles2.less')}";`);
			});	

			it('should return a string without variables - exist in content', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles2.less': fileConfig
				});

				let instance = new File(newConfig2);
				let filename = 'testStyles.less',
					theme = 'build';

				let result = instance.build('testStyles2.less', theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'testStyles2.less')}";`);
			});	

			it('should return a string with theme, variables and file contents - With extra imports', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles.less': fileConfig
				});
				newConfig2.imports = ['testImport'];

				let instance = new File(newConfig2);
				let filename = 'testStyles.less',
					theme = 'build';

				let result = instance.build(filename, theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'dt-test-theme.less')}";\n@import "testImport";\n@imgPath "/images";\n@import "${path.resolve(__dirname, 'build', 'testStyles.less')}";`);
			});	

			it('should return a string with theme, variables and file contents - No imports', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles.less': fileConfig
				});
				delete newConfig2.imports;

				const instance = new File(newConfig2);
				const filename = 'testStyles.less';
				const theme = 'build';

				const result = instance.build(filename, theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'dt-test-theme.less')}";\n@imgPath "/images";\n@import "${path.resolve(__dirname, 'build', 'testStyles.less')}";`);
			});	

			it('should return a string with theme, variables and file contents - No variables', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles.less': fileConfig
				});
				delete newConfig2.variables;

				let instance = new File(newConfig2);
				let filename = 'testStyles.less',
					theme = 'build';

				let result = instance.build(filename, theme);
				expect(result).to.be.a('string')
					.and.to.equal(`@import "${path.resolve(__dirname, 'build', 'dt-test-theme.less')}";\n@import "${path.resolve(__dirname, 'build', 'testStyles.less')}";`);
			});	

			it('should return a string with theme, variables and file contents - No theme', () => {
				let newConfig2 = Object.assign({}, fileConfig, {
					'testStyles.less': fileConfig
				});

				let instance = new File(newConfig2);
				let filename = 'testStyles.less'

				let result = instance.build(filename);
				expect(result).to.be.a('string')
					.and.to.equal(`@imgPath "/images";\n@import "${path.resolve(__dirname, 'build', 'testStyles.less')}";`);
			});	
		});

		describe('resolveFilename fn', () => {
			const instance = new File(fileConfig);

			sinon.stub(instance, 'defineFilename', function() {
				return 'testResult';
			});

			it('should return a string', (done) => {
				let test = instance.resolveFilename('testLessFile.css', 'testcontent', 'testtheme');
				expect(test).to.be.a('string');
				instance.defineFilename.restore();
				done();
			});
		});

		describe('formatFilename fn', () => {
			const instance = new File(fileConfig);

			sinon.stub(instance, 'defineFilename', function() {
				return 'testResult.test';
			});
			
			it('should return a string', (done) => {
				let test = instance.formatFilename(__filename, 'testtheme');
				expect(test).to.be.a('string')
					.and.to.equal('testResult');
				instance.defineFilename.restore();
				done();
			});
		});

		describe('compile fn', () => {
			let config;
			let build;
			let instance;
			let resolveFilename;

			beforeEach(function (done) {
				config = new Config();
				config.set(fileConfig);
				instance = new File(config);

				build = sinon.stub(instance, 'build', function() {
					return ['test'];
				});

				resolveFilename = sinon.stub(instance, 'resolveFilename', function() {
					return 'testStyles.css';
				});
				done();
			});

			afterEach(function (done) {
				instance.build.restore();
				instance.resolveFilename.restore();
				done();
			});
			

			it('should return a promise', () => {
				expect(instance.compile('filename', 'theme')).to.be.a('promise');
			});

			it('should reject the promise if there is an error', () => {
				return instance.compile().catch(e => {
					expect(e).to.be.instanceOf(Error);
				});
			});
		});

		describe('compile fn: should call needed methods based on payload', () => {
			let saveManifest;
			let render;
			let write;
			let config;
			let build;
			let instance;
			let resolveFilename;

			beforeEach(function(done){
				config = new Config();
				config.set(fileConfig);
				instance = new File(config);

				saveManifest = sinon.stub(config, 'saveManifest', function(data) {
					return true;
				});

				render = sinon.stub(instance, 'render', function(data) {
					return new Promise((resolve, reject) => {
						return resolve('testSuccess');
					});
				});

				write = sinon.stub(instance, 'write', function(data) {
					return './build/testStyles.less';
				});

				build = sinon.stub(instance, 'build', function() {
					return ['test'];
				});

				resolveFilename = sinon.stub(instance, 'resolveFilename', function() {
					return 'testStyles.css';
				});

				done();
			});

			afterEach(function (done) {
				config.saveManifest.restore();
				instance.render.restore();
				instance.write.restore();
				instance.build.restore();
				instance.resolveFilename.restore();
				done();
			});

			it('should pass and call all necessary functions', () => {
				instance.compile('testStyles.less', 'testTheme').then(result => {
					expect(build.calledOnce).to.equal(true);
					expect(resolveFilename.calledOnce).to.equal(true);
					expect(render.calledOnce).to.equal(true);
					expect(saveManifest.calledOnce).to.equal(true);
					expect(write.calledOnce).to.equal(true);
				});
			});
		});
	});
});
