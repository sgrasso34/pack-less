'use strict';

const path = require('path');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const Config = require('../src/config.js');
const PackLess = require('../index.js');

const blankOptions = {
	resolve: {
		root: './1',
		prefix: './2',
		filename: './3',
		ext: './4'
	},
	output: {
		path: './5'
	},
	manifest: {
		path:  path.join(__dirname, './build'),
		filename: 'testManifest.json'
	},
	renderOptions: {
		paths: [
			'./8',
			'./9'
		],
		compress: true
	},
	theme: {
		path: '',
		filename: 'dt-theme.less'
	},
	variables: {
		imgPath: './12' //global inject in all files
	},
	files: [
		'./13'
	]
};

describe('PackLess', () => {
	describe('constructor', () => {
		it('should be a function', () => {
			expect(PackLess).to.be.a('function');
		});
		it('should return and instance of itself', () => {
			expect(new PackLess(blankOptions)).to.be.instanceOf(PackLess);
		});
		it('requires a properly-configured object (options) as a parameter and return empty array', () => {
			let testPackLess = new PackLess(blankOptions);
			expect(testPackLess).to.be.empty;
		});
		it('throw error if options is not an object', () => {
			expect(() => new PackLess('blankOptions')).to.throw('Invalid argument: options');
		});
		it('throw error if options is an object but does not have the output property', () => {
			let opts = Object.assign({}, blankOptions);
			delete opts.output;
			expect(() => new PackLess(opts)).to.throw('Invalid argument: options');
		});
	});

	describe('pack', () => {
		it('should be a function', () => {
			let testPackLess = new PackLess(blankOptions);
			expect(testPackLess.pack).to.be.a('function');
		});
		it('should return a promise and call associated methods', sinon.test(() => {
			let testPackLess = new PackLess(blankOptions);
			sinon.stub(testPackLess.config, 'process', function(x) {
				return true;
			});	
			sinon.stub(testPackLess.config, 'merge', function() {
				return true;
			});	
			sinon.stub(testPackLess.config, 'prep', function(file) {
				return file;
			});

			testPackLess.pack(['build/testStyles.less']).then(d => {
				expect(testPackLess.config.process.calledOnce).to.equal(true);
				expect(testPackLess.config.merge.calledOnce).to.equal(true);
				expect(testPackLess.config.prep.calledOnce).to.equal(true);
			});
		}));
	});
});