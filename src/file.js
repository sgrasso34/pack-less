'use strict';

const path = require('path');
const fs = require('fs');
const less = require('less');
const crypto = require('crypto');

/**
 * @private
 * @type {WeakMap}
 */
const CONFIG = new WeakMap();

/**
 * @class
 */
class File {

	/**
	 * @constructor
	 * @param {object} config set or override default options
	 */
	constructor(config) {
		CONFIG.set(this, config);
	}

	/**
	 * @readonly
	 * @type {PackLessConfig}
	 */
	get config() {
		return CONFIG.get(this);
	}

	/**
	 * @param {String} file - file path to read
	 * @returns {file} read file response
	 */
	read(file) {
		return fs.readFileSync(file, 'utf8');
	}

	/**
	 * @param {String} filePath - name of filePath to write to
	 * @param {String} filename - name of filename to write
	 * @param {String} content - file contents
	 * @param {String} theme - file theme
	 * @returns {Promise} Promise Object resolves after file write
	 */
	write(filePath, filename, content, theme) {
		return new Promise((resolve, reject) => {
			fs.writeFile(filePath, content, e => {
				if (e != null) return reject(e);
				this.addToManifest(filename, filePath, theme);
				return resolve(filePath);
			});
		});
	}

	resolveFilename(filename, content, theme){
		return path.resolve(this.config.output.path, this.defineFilename(filename, theme, content));
	}

	formatFilename(filename, theme){
		return this.defineFilename(filename, theme).slice(0, -4).replace(/.$/, '');
	}

	/**
	 * @param {String} filename - file name
	 * @param {String} filePath - file path
	 * @param {String} theme - file theme
	 * @returns {Object} manifestOutput
	 */
	addToManifest(filename, filePath, theme){
		this.config.manifestOutput[this.formatFilename(filename, theme)] = path.basename(filePath);
		return this.config.manifestOutput;
	}

	/**
	 * @param {String} fileContent - file contents
	 * @returns {output} Hash of contents
	 */
	createHash(fileContent){
		return crypto.createHash('md5').update(fileContent, 'utf8').digest('hex');
	}

	/**
	 * @param {String} filename - name of file
	 * @param {String} theme - theme of file
	 * @param {String|Undefined} fileContent - contents of file
	 * @returns {String} modified file name
	 */
	defineFilename(filename, theme, fileContent){
		return this.config.resolve.filename
					.replace(/\[theme\]/g, (theme) ? theme : '')
					.replace(/\[prefix\]/g, this.config.resolve.prefix)
					.replace(/\[name\]/g, filename.replace(/\//g, '-').replace('.less', ''))
					.replace(/\[hash\]/g, (fileContent) ? this.createHash(fileContent) : '')
				+ this.config.resolve.ext;
	}

	/**
	 * @param {String} filename - name of file
	 * @param {String} content - contents of file
	 * @param {String} theme - theme of file
	 * @returns {String} output.css css output
	 */
	render(filename, content, theme) {
		return new Promise((resolve, reject) => {
			let options = JSON.parse(JSON.stringify(this.config.renderOptions));

			if (theme)
				options.paths.push(path.resolve(this.config.theme.path, theme));

			options.paths.push(path.resolve(this.config.resolve.root));

			less.render(content, options, (e, output) => {
				if (e != null) return reject(e);
				return resolve(output.css);
			});
		});
	}

	/**
	 * @param {String} filename - name of file
	 * @param {String} theme - theme of file
	 * @returns {String} CSS string to compile
	 */
	build(filename, theme) {
		let strArr = [];
		const filePath = path.join(this.config.resolve.root, filename);
		const fileContent = this.read(filePath);

		//Imports
		if (theme){
			if (fileContent.indexOf(this.config.theme.filename) === -1)
				strArr.push('@import' + ' "' + path.resolve(this.config.theme.path, theme, this.config.theme.filename) + '";\n');
		}

		if (this.config.imports){
			for (const val of this.config.imports){
				if (fileContent.indexOf(val) === -1)
					strArr.push('@import' + ' "' + val + '";\n');
			}
		}

		if (this.config.variables){
			//Variables
			for (const value in this.config.variables){
				if (fileContent.indexOf(value) === -1)
					strArr.push('@' + value + ' "' + this.config.variables[value] + '";\n');
			}
		}

		return strArr.join('') + '@import' + ' "' + filePath + '";';
	}

	/**
	 * @param {String} file - name of file
	 * @param {String} theme - theme of file
	 * @returns {Promise} Promise Object resolves rendering and writing file and writing/updating manifest
	 */
	compile(file, theme) {
		return this.render(file, this.build(file, theme), theme).then(value => {
			return this.write(this.resolveFilename(file, value, theme), file, value, theme);
		}).then(() => {
			return this.config.manifestOutput;
		});
	}
}

module.exports = File;