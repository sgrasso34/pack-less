'use strict';

const path = require('path');

const defaultResolver = [
	/**
	 * `require('<module-name>')`
	 * @param {String} value
	 * @returns {String|Null}
	 */
	value => {
		try {
			return require.resolve(value);
		} catch (e) {
			return e;
		}
	},

	/**
	 * `path.resolve('<file-name>')`
	 * @param {String} value
	 * @returns {String}
	 */
	value => {
		path.resolve(value);
	}
];

/**
 * @class
 */
class PathResolver {

	/**
	 * @param {String} value - config file path to resolve
	 * @returns {String|Null} Fully Resolved path or Null
	 */
	resolve(value) {
		for (const resolver of defaultResolver) {
			let resolved = resolver(value), isNull = (resolved instanceof Error);
			if (typeof resolved === 'string' && !isNull) {
				value = resolved;
				break;
			}
		}
		return value;
	}
}

module.exports = PathResolver;