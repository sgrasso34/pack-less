'use strict';

const fs = require('fs');
const path = require('path');

const PathResolver = require('./resolver.js');
const File = require('./file.js');

/**
 * @private
 * @type {WeakMap}
 */
const PATH_RESOLVER = new WeakMap();

/**
 * @class
 */
class Config {

	/**
	 * @constructor
	 */
	constructor() {
		PATH_RESOLVER.set(this, new PathResolver());

		//Defaults
		this.defaults = {
			configName : 'packless.config.js',
			imports : [],
			variables : {},
			renderOptions : {
				paths: [],
				compress: true
			},
			manifest : {
				path: './',
				filename: 'packless.manifest.json'
			},
			manifestOutput : {}
		}
	}

	/**
	 * @readonly
	 * @type {DoLessPathResolver}
	 */
	get pathResolver() {
		return PATH_RESOLVER.get(this);
	}

	/**
	 * @param {String} pattern - config file to find
	 * @returns {String} full file path
	 */
	find(pattern) {
		pattern = path.parse(this.pathResolver.resolve(pattern)).dir;
		let filePath = null;

		try {
			let files = fs.readdirSync(pattern);

			for (const filename of files){
				if (filename === this.configName){
					filePath = path.join(pattern, filename);
				}
			}

			return filePath;
		} catch (e) {
			return new Error(e);
		}
	}

	/**
	 * @param {String} pattern - config file to find
	 * @returns {String} closest file to pattern resolved
	 */
	findClosest(pattern) {
		if (path.extname(pattern) === '.js'){
			return this.pathResolver.resolve(pattern);
		}
		const config = this.find(pattern);

		if (config != null && config instanceof Error === false) {
			return this.pathResolver.resolve(config);
		}
		throw new Error('Config Not Found: Expecting ' + this.configName);
	}

	/**
	 * @param {object} config - Current config
	 * @returns {Object} fileconfig
	 */
	prep(config) {
		return require(this.findClosest(config));
	}

	/**
	 * @param {String} themeRoot - path to themes directory
	 * @returns {String[]} themes
	 */
	getThemes(themeRoot) {
		let themes = [];

		if (!themeRoot || !themeRoot.length) return themes;

		fs.readdirSync(themeRoot).forEach(theme => {
			themes.push(theme);
		});

		return themes;
	}

	/**
	 * @param {Object|Undefined} obj - object to clone
	 * @returns {Object} copy
	 */
	clone(obj) {
		let copy;

		// Handle the 3 simple types, and null or undefined
		if (obj == null || typeof obj != 'object') return obj;

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			for (let i = 0, len = obj.length; i < len; i++) {
				copy[i] = this.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (const attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = this.clone(obj[attr]);
			}
			return copy;
		}

		throw new Error('Its type isn\'t a supported to clone.');
	}

	/**
	 * @param {Object|Undefined} obj - object to set
	 * @param {string} key - key to set
	 * @returns {object} - key end result
	 */
	set(obj, key) {
		if (key in this){
			key = this[key];
		} else if (key != null) {
			this[key] = {};
			key = this[key];
		} else {
			key = this;
		}

		for (const prop in obj){
			key[prop] = this.clone(obj[prop]);
		}
		return key;
	}

	/**
	 * @param {Object} src - target
	 * @param {Object} obj - object to be merged
	 * @returns {Object} merged object
	 */
	merge(src, obj){
		let copy = this.clone(src);
		for (const prop in obj){
			copy[prop] = this.clone(obj[prop]);
		}
		return copy;
	}

	/**
	 * @param {Object} fileConfig - Config file to process
	 * @returns {Promise} Promise Object resolves after process completes all compilation
	 */
	process(fileConfig){
		return Promise.all(fileConfig.files.map(file => {

			let theFile = new File(fileConfig);

			if (fileConfig.theme){
				const themes = this.getThemes(fileConfig.theme.path);
				return Promise.all(themes.map(theme => {
					return theFile.compile(file, theme).then(data => {
						this.defaults.manifestOutput = Object.assign(this.defaults.manifestOutput, data);
						return this.defaults.manifestOutput;
					});
				}));
			}

			return theFile.compile(file).then(data => {
				this.defaults.manifestOutput = Object.assign(this.defaults.manifestOutput, data);
				return this.defaults.manifestOutput;
			});
		}));
	}


	/**
	 * @returns {Promise} Promise Object resolves after file write
	 */
	saveManifest(){
		return new Promise((resolve, reject) => {
			let filePath = path.resolve(this.defaults.manifest.path, this.defaults.manifest.filename);
			fs.writeFile(filePath, JSON.stringify(this.defaults.manifestOutput), e => {
				if (e != null) return reject(e);
				return resolve(this.defaults.manifestOutput);
			});
		});
	}
}

module.exports = Config;