'use strict';

const Config = require('./src/config.js');

/**
 * @private
 * @type {WeakMap}
 */
const CONFIG = new WeakMap();

/**
 * @class
 */
class PackLess {

	/**
	 * @constructor
	 * @param {object} options set and override default options
	 * @returns {null} null
	 */
	constructor(options) {

		CONFIG.set(this, new Config());

		if (typeof options !== 'object' || !options.output) {
			throw new Error('Invalid argument: options');
		}

		this.config.set(
			this.config.merge(
				this.config.defaults,
				options
			), 'defaults'
		);

		if (Array.isArray(options.files) && options.files.length){
			this.config.process(this.config.defaults)
				.then(() => {
					return this.config.saveManifest();
				}).then(val => {
					console.log('Success! Saved:');
					console.log(Object.keys(val));
				}).catch(err => {
					console.error(err);
				}
			);
		}
		return this;
	}

	/**
	 * @readonly
	 * @type {CONFIG}
	*/
	/* istanbul ignore next */
	get config() {
		return CONFIG.get(this);
	}

	/**
	 * @param {array} configs config file locations
	 * @returns {PackLess} Promise Object
	 */
	pack(configs) {
		return Promise.all(configs.map(config => {
			return this.config.process(
						this.config.merge(
							this.config.defaults,
							this.config.prep(config)
						)
					);
		})).then(() => {
			return this.config.saveManifest();
		}).then(val => {
			console.log('Success! Saved:');
			console.log(Object.keys(val));
			return val;
		}).catch(err => {
			console.error(err);
			return err;
		});
	}
}

module.exports = PackLess;